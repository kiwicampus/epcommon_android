package co.sanduche.util

import android.app.Activity
import android.content.Context
import android.content.Intent

import android.os.Bundle
import kotlin.properties.Delegates
import android.app.Fragment
import android.view.KeyEvent
import android.net.Uri
import java.io.PrintWriter
import java.io.FileDescriptor
import android.content.ComponentName
import android.view.Menu
import android.view.MenuItem
import android.support.v4.app.FragmentActivity

open class ActivityView(val activity: ActivityPresenter) {

    fun setContentView(layoutId: Int) = activity.setContentView(layoutId)
    fun findViewById(id: Int) = activity.findViewById(id)
    fun getMenuInflater() = activity.getMenuInflater();
    open fun onCreate() {

    }

    open fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return activity.onCreateOptionsMenu(menu)
    }
    open fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        return activity.onPrepareOptionsMenu(menu)
    }
    open fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return activity.onOptionsItemSelected(item)
    }


}

open class ActivityPresenter() : FragmentActivity() {
    var view: ActivityView? = null

    override fun onCreate(bundle: Bundle?) {
        onCreate(bundle)
        view?.onCreate();
    }


}


fun main(args: Array<String>) {

    val x = javaClass<Activity>();
    x.getDeclaredMethods().forEach {
        val name = it.getName()
        fun Class<*>.toTypeName(): String {

            if (this.isArray()) {
                return "Array<${this.getSimpleName().capitalize().replaceAll("""\[\]""", "")}>"
            } else {
                return this.getSimpleName().capitalize()
            }
        }

        val paramsDeclr = it.getParameterTypes().map { "${it.getSimpleName().toLowerCase()}:${it.toTypeName()}" }.join(separator = ", ")
        val paramsInvoke = it.getParameterTypes().map { it.getSimpleName().toLowerCase() }.join(separator = ", ")
        val returnTye = it.getReturnType().toTypeName()
        if (name.startsWith("on"))
            println("open fun $name($paramsDeclr):$returnTye?{return null}")
        else {
            println("fun $name($paramsDeclr):$returnTye=activity.$name($paramsInvoke)")
        }
    }
}
