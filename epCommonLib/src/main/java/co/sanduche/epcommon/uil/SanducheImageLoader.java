package co.sanduche.epcommon.uil;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import co.sanduche.epcommon.app.SanducheApp;
import co.sanduche.epcommon.util.network.FileDownloader;
import co.sanduche.epcommon.vos.Tuple;

/**
 * Created by Yeisson on 26/03/2015.
 * <p/>
 * As of UIL 1.9.3, ImageLoader implementation does not allow to load images in
 * specific sizes. Instead, it tries to determine image size by inspecting
 * View size. This mechanism does not work properly when view size has not been specified
 * in layout file, for instance, when android:layout_width/android:layout_height are set
 * to 'match_parent'. As a workaround, this class is used to create new ImageLoaders for
 * every image size required. Of course this approach is not recommended when many
 * ImageLoader/image sizes are required, as each ImageLoader can be resource consuming (mainly threads)
 * <p/>
 * This behavior is expected to change in UIL 1.9.4
 * <p/>
 * //TODO Go back to default implementation as soon as UIL 1.9.4 is released
 */
public class SanducheImageLoader extends ImageLoader {

    private static Map<Tuple<Integer, Integer>, SanducheImageLoader> instances = new HashMap<Tuple<Integer, Integer>, SanducheImageLoader>();

    protected SanducheImageLoader() {
        super();
    }

    public static SanducheImageLoader getInstance(int targetMaxWidth, int targetMaxHeight) {
        Tuple<Integer, Integer> key = new Tuple<Integer, Integer>(targetMaxWidth, targetMaxHeight);
        if (instances.containsKey(key))
            return instances.get(key);

        String CACHE_DIR = SanducheApp.CACHE_DIR;
        File cacheDir = StorageUtils.getOwnCacheDirectory(SanducheApp.getInstance(), CACHE_DIR);

        //Default image display options
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                //Scale type
                .imageScaleType(ImageScaleType.EXACTLY)
                        //This configuration slightly reduces image quality, and greatly improve memory consumption
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .displayer(new FadeInBitmapDisplayer(150))
                .resetViewBeforeLoading(true)
                .considerExifParams(true)
                .build();
        //By default, memory cache is enabled (LruMemoryCache)
        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
                SanducheApp.getInstance())
                .defaultDisplayImageOptions(defaultOptions)
                        //Configure disk cache
                .diskCache(new SanducheDiskCache(cacheDir))
//                .imageDownloader(new BaseImageDownloader(SanducheApp.getInstance(), 5000, 20000))
                        //Max size of images loaded with UIL.
                .memoryCacheExtraOptions(targetMaxWidth, targetMaxHeight)
                .threadPoolSize(5)
//                .writeDebugLogs()
                ;

        ImageLoaderConfiguration config = builder.build();
        SanducheImageLoader imageLoader = new SanducheImageLoader();
        imageLoader.init(config);

        instances.put(key, imageLoader);
//        imageLoader.clearDiskCache();
        return imageLoader;
    }

    public void displayVideoThumbnail(String url, int playResource, ImageView view, ProgressBar progressBar) {
        url = url.replace("https:", "http:");
        File file = getDiskCache().get(url);
        //Check file and thumbnail exist
        File thumb = file != null ? new File(file.getAbsolutePath() + "_thumbnail") : null;

        if (file != null && thumb != null && file.exists() && thumb.exists())
            display("file://" + thumb.getAbsolutePath(), view, progressBar);
        else {
            if (progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
                view.setVisibility(View.GONE);
            } else
                view.setVisibility(View.VISIBLE);
            new VideoDownloadTask(playResource, url, view, progressBar).execute();
        }
    }

    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
//        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Bitmap bmOverlay = Bitmap.createBitmap(500, 500, bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, null, new Rect(0,0, 500,500), null);
//        canvas.drawBitmap(bmp2, bmp1.getWidth()/2 - bmp2.getWidth()/2, bmp1.getHeight()/2 - bmp2.getHeight()/2, null);
        canvas.drawBitmap(bmp2, null, new Rect(500/4, 500/4,(500*3)/4, (500*3)/4), null);
        return bmOverlay;
    }

    protected class VideoDownloadTask extends AsyncTask<Void, Integer, String> {

        private final ImageView view;
        private final ProgressBar progressBar;
        int resource;
        String url;
        private Exception exception;

        public VideoDownloadTask(int resource, String url, ImageView view, ProgressBar progressBar) {
            this.resource = resource;
            this.url = url;
            this.view = view;
            this.progressBar = progressBar == null ? new ProgressBar(null) : progressBar;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setMax(110);
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                publishProgress(0);
                File videoFile = getDiskCache().get(url);
                String localPath = FileDownloader.downloadFileSync(url, videoFile.getAbsolutePath(), new FileDownloader.EPProgressListener<String>() {
                    @Override
                    public void onProgressUpdate(double progress) {
                        progressBar.setProgress((int) progress);
                    }
                });
                File thumbFile = new File(localPath + "_thumbnail");
                Log.i("SanducheImageLoader", "URL " + url + (videoFile.exists() ? " successfully" : " not") + " downloaded to " + localPath);

                //It had been created before
                if (thumbFile.exists())
                    return "file://" + thumbFile;
//                        thumbFile.delete();

                //Otherwise, create thumbnail

                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(localPath, MediaStore.Video.Thumbnails.MINI_KIND);
                if (thumb == null)
                    thumb = loadImageSync("assets://empty.png");
//                publishProgress(103);
                Bitmap play = BitmapFactory.decodeResource(SanducheApp.getInstance().getResources(), resource);
                Bitmap overlay = overlay(thumb, play);
//                publishProgress(107);
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(thumbFile);
                    overlay.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                    // PNG is a loss less format, the compression factor (100) is ignored
                    return "file://" + thumbFile;
                } finally {
                    if (out != null)
                        out.close();
                    publishProgress(110);
                }
            } catch (Exception e) {
                e.printStackTrace();
                exception = e;
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values[0]);
        }

        @Override
        protected void onPostExecute(String fileUri) {
            if (exception != null) {
                return;
            }
            Log.i("SanducheImageLoader", "Thumbnail created, ready to display: " + fileUri);
            display(fileUri, view, progressBar);
        }
    }

    @Override
    public void displayImage(String uri, ImageAware imageAware, DisplayImageOptions options, ImageLoadingListener listener, ImageLoadingProgressListener progressListener) {
        super.displayImage(uri == null ? null : uri.replace("https:", "http:"), imageAware, options, listener, progressListener);
    }

    public void display(final String thumbFileUri, ImageView view, final ProgressBar progressBar) {
        Log.i("SanducheImageLoader", "Displaying: " + thumbFileUri);
        displayImage(thumbFileUri, view, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                Log.i("SanducheImageLoader", "loading started: " + thumbFileUri);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                Log.i("SanducheImageLoader", "loading failed: " + thumbFileUri);
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                Log.i("SanducheImageLoader", "loading cancelled: " + thumbFileUri);
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
            }
        });
    }

}
