package co.sanduche.epcommon.uil;

import android.graphics.Bitmap;
import android.util.Log;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.utils.IoUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Yeisson on 19/03/2015.
 * UnlimitedDiscCache implementation saves to disks all images, including those which are already
 * on disk. This implementation checks for URI to start with file:// - In that case, image is not
 * copied to cache dir
 */
public class SanducheDiskCache extends  UnlimitedDiskCache {


    public static final String TAG = SanducheDiskCache.class.getSimpleName();

    public SanducheDiskCache(File cacheDir) {
        super(cacheDir);
    }

    public void purgeCache(){
        boolean a = cacheDir.delete();
        if(a)
            cacheDir.mkdir();
        else
            System.out.println("Cannot purge disk cache");
    }

    public SanducheDiskCache(File cacheDir, File reserveCacheDir) {
        super(cacheDir, reserveCacheDir);
    }

    @Override
    public File get(String imageUri) {
        File file;
        file = imageUri.startsWith("file://") ? new File(imageUri.replaceFirst("file://", "")) : super.get(imageUri);
        Log.i(TAG, String.format("URI %s in disk cache: %s, file: %s ", (file != null && file.exists() ? "found" : "NOT found"), imageUri, file.getPath()));
        return file;

    }

    @Override
    public boolean save(String imageUri, Bitmap bitmap) throws IOException {
        Log.v("SanducheDiskCache", String.format("Bitmap URI saved to disk cache: %s", imageUri));
        return super.save(imageUri, bitmap);
    }

    @Override
    public boolean save(String imageUri, InputStream imageStream, IoUtils.CopyListener listener) throws IOException {
        Log.v("SanducheDiskCache", String.format("Stream URI saved to disk cache: %s", imageUri));
        return super.save(imageUri, imageStream, listener);
    }
}
