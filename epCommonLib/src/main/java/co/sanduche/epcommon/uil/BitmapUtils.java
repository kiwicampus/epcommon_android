package co.sanduche.epcommon.uil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.util.Log;

/**
 * http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
 */
public class BitmapUtils {

	private static final String TAG = BitmapUtils.class.getSimpleName();

	/**
	 * Decodes a bitmap from an Uri. It automatically calculates the necessary
	 * parameters for BitmapFactory to use.
	 * 
	 * @param context
	 * @param imageUri
	 *            Must be a file uri. The method cannot decode media uri's
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */


	private static BitmapFactory.Options imageBoundsInfo(Context context,
			Uri imageUri) {

		InputStream in;
		try {
			in = context.getContentResolver().openInputStream(imageUri);
		} catch (FileNotFoundException e) {
			Log.w(TAG, "file not found", e);
			return null;
		}

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(in, null, options);

		try {
			in.close();
		} catch (IOException e) {
			Log.e(TAG, "error closing InputStream", e);
		}
		return options;
	}

	public static Bitmap decodeSampledBitmapFromUri(Context context,
			Uri imageUri, int reqWidth, int reqHeight) {
		Log.d(TAG, "imageUri=" + imageUri);

		Options options = imageBoundsInfo(context, imageUri);
		InputStream in;
		try {
			in = context.getContentResolver().openInputStream(imageUri);
		} catch (FileNotFoundException e) {
			Log.w(TAG, "file not found", e);
			return null;
		}

		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		options.inJustDecodeBounds = false;
		options.inPurgeable = true; // http://stackoverflow.com/questions/7068132/why-would-i-ever-not-use-bitmapfactorys-inpurgeable-option
		options.inInputShareable = false; // InputStream is not reusable
		try {
			return BitmapFactory.decodeStream(in, null, options);
		} finally {
			try {
				in.close();
			} catch (IOException ignore) {
			}
		}
	}

	private static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		Log.d(TAG, "height=" + height + " width=" + width);

		if (height > reqHeight || width > reqWidth) {
			float heightRel=(float) height / (float) reqHeight;
			float widthRel=(float) width / (float) reqWidth;
			if (heightRel >	 widthRel) {
				inSampleSize = Math.round(widthRel);
			} else {
				inSampleSize = Math.round(heightRel);
			}
		}
		return inSampleSize;
	}

	public static String getPathFromMediaUri(Context context, Uri _uri) {
		String filePath;
		if (_uri != null && "content".equals(_uri.getScheme())) {
			Cursor cursor = context
					.getContentResolver()
					.query(_uri,
							new String[] { android.provider.MediaStore.Images.ImageColumns.DATA },
							null, null, null);
			cursor.moveToFirst();
			filePath = cursor.getString(0);
			cursor.close();
		} else {
			filePath = _uri.getPath();
		}
		return filePath;
	}
}
