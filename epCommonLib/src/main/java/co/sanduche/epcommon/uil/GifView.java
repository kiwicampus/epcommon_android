package co.sanduche.epcommon.uil;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

public class GifView extends View{

	private Movie gifMovie;
	private int movieWidth, movieHeight;
	private long movieDuration;
	private long movieStart;
	
	public GifView(Context context) {
		super(context);
//		init(context);
	}

	
	public GifView(Context context, AttributeSet attrs) {
		super(context, attrs);
//		init(context);
	}
	
	public GifView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
//		init(context);
	}
	
	public void init(Context context, int resource) {
		setFocusable(true);
		InputStream gifInputStream = context.getResources().openRawResource(resource);
		
		gifMovie = Movie.decodeStream(gifInputStream);
		movieWidth = gifMovie.width();
		movieHeight = gifMovie.height();
		movieDuration = gifMovie.duration();
		invalidate();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
	}
	
	public int getMovieWidth() {
		return movieWidth;
	}
	
	public int getMovieHeight() {
		return movieHeight;
	}
	
	public long getMovieDuration() {
		return movieDuration;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		
		long now = SystemClock.uptimeMillis();
		
		if(movieStart == 0) {
			movieStart = now;
		}
		
		if(gifMovie != null) {
			
			int dur = gifMovie.duration();
			if(dur == 0) {
				dur = 1000;
			}
			
			int relTime = (int)((now - movieStart) % dur);
			
			gifMovie.setTime(relTime);
			float scaledWidth = (float) this.getWidth() / (float) gifMovie.width();
			float scaledHeight = (float) this.getHeight() / (float) gifMovie.height();
			canvas.scale(scaledWidth, scaledHeight);
			gifMovie.draw(canvas, 0, 0);
			invalidate();
		}
	}
}
