package co.sanduche.epcommon.uil;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper that encapsulates Json read/write logic.
 * This is mostly used ease change the library if needed
 *
 * Created by jasonoviedo on 2/1/17.
 */

public class EPJson {

    private static final ObjectMapper mapper = new ObjectMapper();
    static{
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static Map<String, Object> jsonToMap(String data) {
        try {
            return mapper.readValue(data, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
            return new HashMap<String,Object>();
        }
    }

    public static<T> T fromJson(String jsonString, Class<T> responseType) throws IOException {
        return (T) mapper.readValue(jsonString, responseType);
    }

    public static String toJson(Object requestData) {
        try {
            return mapper.writeValueAsString(requestData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @NonNull
    public static<T> List<T> fromJsonArray(String jsonString, Class<T> responseType) {
        try {
            List<Map> list = null;
            list = (List<Map>) mapper.readValue(jsonString, new TypeReference<List<Map>>() {
            });
            List<T> res = new ArrayList<T>();//                res = (List<T>) JSON.std.listOfFrom(responseType, jsonString);
            for (Map m : list)
                res.add((T) mapper.convertValue(m, responseType));
            return res;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
