package co.sanduche.epcommon.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import co.sanduche.R;

public abstract class EPListWithSectionsAdapter extends BaseAdapter {

    private Context c;

    public EPListWithSectionsAdapter(Context c) {
        this.c = c;
        init();
    }

    @Override
    public void notifyDataSetChanged() {
        init();
        super.notifyDataSetChanged();
    }

    public abstract int getNumSections();

    public abstract int getNumItemsInSection(int section);

    /**
     * If you need some sections not to have a header, just return co.sanduche.R.layout.list_empty_header
     * @param section Section number
     * @return Layout to be used for such section
     */
    public int getSectionHeaderLayout(int section){
        return R.layout.list_empty_header;
    }

    public abstract int getItemLayout(int section, int item);

//    @Override
//    final public int getViewTypeCount() {
//        return resources.length;
//    }

    private List<AbstractItemContainer> items = new ArrayList<AbstractItemContainer>();

    @Override
    final public int getCount() {
        return items.size();
    }

    public void init() {
        items.clear();
        int sec = getNumSections();
        int numItems;
        for (int i = 0; i < sec; i++) {
            items.add(new AbstractItemContainer(i));
            numItems = getNumItemsInSection(i);
            for (int j = 0; j < numItems; j++)
                items.add(new AbstractItemContainer(i, j));
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AbstractItemContainer abstractItem = items.get(position);
        int sectionNumber = abstractItem.sectionNumber;
        int resource = abstractItem.isSectionHeader? getSectionHeaderLayout(sectionNumber) : getItemLayout(sectionNumber, abstractItem.itemNumber);
        View view = (convertView != null && ((int)convertView.getTag()) == resource ? convertView : createView(position,
                parent));
        if (abstractItem.isSectionHeader)
            initHeaderView(sectionNumber, view);
        else
            initItemView(sectionNumber, abstractItem.itemNumber, view);
        return view;
    }

    public void initHeaderView(int sectionNumber, View view){

    }

    public abstract void initItemView(int sectionNumber, int itemNumber, View view);

    public View createView(int position, ViewGroup parent) {
        if (c == null)
            throw new RuntimeException(
                    "No se ha suministrado suficiente info para crear la vista automáticamente");
        LayoutInflater inflater = (LayoutInflater) c
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        AbstractItemContainer abstractItem = items.get(position);
        int resource = abstractItem.isSectionHeader ? getSectionHeaderLayout(abstractItem.sectionNumber) : getItemLayout(abstractItem.sectionNumber, abstractItem.itemNumber);
        View view = inflater.inflate(resource, parent, false);
        view.setTag(resource);
        abstractItem.layout = resource;
        if (abstractItem.isSectionHeader)
            postCreateSection(abstractItem.sectionNumber, view);
        else
            postCreateSection(abstractItem.sectionNumber, abstractItem.itemNumber, view);
        return view;
    }

    public void postCreateSection(int sectionNumber, View view) {

    }

    public void postCreateSection(int sectionNumber, int itemNumbers, View view) {

    }

    public AbstractItemContainer getAbstractItem(int position) {
        return items.get(position);
    }


    public static class AbstractItemContainer {
        public boolean isSectionHeader;
        public int sectionNumber;
        public int itemNumber;
        protected int layout;

        public AbstractItemContainer(int sectionNumber) {
            this.isSectionHeader = true;
            this.sectionNumber = sectionNumber;
        }

        public AbstractItemContainer(int sectionNumber, int itemNumber) {
            this.isSectionHeader = false;
            this.sectionNumber = sectionNumber;
            this.itemNumber = itemNumber;
        }
    }
}
