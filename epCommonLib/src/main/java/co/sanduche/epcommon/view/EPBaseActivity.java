package co.sanduche.epcommon.view;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;

public class EPBaseActivity extends FragmentActivity{

    private String actionBarFont;
    protected String title;

//    public EPBaseActivity(String title) {
//		this.title = title;
//	}

    public EPBaseActivity(String title, String actionBarFont) {
        this.title = title;
        this.actionBarFont = actionBarFont;
    }

	@Override
	protected void onCreate(Bundle arg0) {
		setTitle(title);

		super.onCreate(arg0);
	}

	protected void setTitle(String title) {
		this.title = title;
		SpannableString s = new SpannableString(title);
		s.setSpan(new TypefaceSpan(this, actionBarFont!=null? actionBarFont:"", true), 0,
				s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		// Update the action bar title with the TypefaceSpan instance
		ActionBar actionBar = getActionBar();
		actionBar.setTitle(s);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				// This activity is NOT part of this app's task, so create a
				// new
				// task
				// when navigating up, with a synthesized back stack.
				TaskStackBuilder.create(this)
				// Add all of this activity's parents to the back stack
						.addNextIntentWithParentStack(upIntent)
						// Navigate up to the closest parent
						.startActivities();
			} else {
				// This activity is part of this app's task, so simply
				// navigate up to the logical parent activity.
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
