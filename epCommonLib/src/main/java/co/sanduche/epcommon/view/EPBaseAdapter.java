package co.sanduche.epcommon.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class EPBaseAdapter extends BaseAdapter {

	private Context c;
	private Integer[] resources;

//	public EPBaseAdapter() {
//
//	}

	public EPBaseAdapter(Context c, Integer... resources) {
		this.c = c;
		this.resources = resources;
	}


    @Override
    final public int getViewTypeCount() {
        return resources.length;
    }

    @Override
	public abstract int getCount();

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = (convertView != null ? convertView : createView(position,
				parent));
		initView(position, view);
		return view;
	}

	public abstract void initView(int position, View view);

	public View createView(int position, ViewGroup parent) {
		if (c == null || resources == null || resources.length == 0)
			throw new RuntimeException(
					"No se ha suministrado suficiente info para crear la vista automáticamente");
		LayoutInflater inflater = (LayoutInflater) c
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Integer resource = this.resources[getItemViewType(position)];
        View view = inflater.inflate(resource, parent, false);
		postCreate(position, view);
		return view;
	}

	public void postCreate(int position, View view){

	}

}
