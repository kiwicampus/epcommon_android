package co.sanduche.epcommon.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.widget.Toast;

public class DialogUtil {

    public static void alert(android.app.Fragment context, final String message) {
            try {
                AlertDialogFragment.newInstance(message)
                        .show(context.getFragmentManager(), "alert");
            }catch (Exception e){
                e.printStackTrace();
            }
    }

    public static void alert(android.support.v4.app.Fragment context,
                             final String message) {
        try {
            AlertSupportDialogFragment.newInstance(message)
                    .show(context.getFragmentManager(), "alert");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void alert(Activity context, final String message) {
        try {
            AlertDialogFragment.newInstance(message)
                    .show(context.getFragmentManager(), "alert");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void message(final Activity context, @StringRes final int messageResource) {
        final String message = context.getString(messageResource);
        message(context, message);
    }

    @Deprecated
    public static void message(final Activity context, final String message) {
        try {
            context.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void shortMessage(final Activity context, @StringRes final int messageResource) {
        final String message = context.getString(messageResource);
        shortMessage(context, message);
    }


    public static void shortMessage(final Activity context, final String message) {
        try {
            context.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static ProgressDialog showBusy(Activity context, String message) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message != null ? message : "Loading");
        progressDialog.show();
        return progressDialog;
    }

    public static class AlertDialogFragment extends DialogFragment {

        public static AlertDialogFragment newInstance(String title) {
            AlertDialogFragment frag = new AlertDialogFragment();
            Bundle args = new Bundle();
            args.putString("title", title);
            frag.setArguments(args);
            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String title = getArguments().getString("title");

            return new AlertDialog.Builder(getActivity())
                    .setTitle(title).setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    })
                    .create();
        }

    }

    public static class AlertSupportDialogFragment extends android.support.v4.app.DialogFragment {

        public static AlertSupportDialogFragment newInstance(String title) {
            AlertSupportDialogFragment frag = new AlertSupportDialogFragment();
            Bundle args = new Bundle();
            args.putString("title", title);
            frag.setArguments(args);
            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String title = getArguments().getString("title");

            return new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setTitle(title).setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            })
                    .create();
        }

    }
}
