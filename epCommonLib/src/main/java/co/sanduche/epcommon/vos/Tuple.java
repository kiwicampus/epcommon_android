package co.sanduche.epcommon.vos;

import java.io.Serializable;

public class Tuple<X, Y> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private X x;
    private Y y;

    public Tuple(X x, Y y) {
        this.setX(x);
        this.setY(y);
    }

    public X getX() {
        return x;
    }

    public void setX(X x) {
        this.x = x;
    }

    public Y getY() {
        return y;
    }

    public void setY(Y y) {
        this.y = y;
    }

    /**
     * Tuple equality is defined as:<br/>
     * <br/>
     * <code>
     * Tuple a,b;
     *<br/>
     * a.equals(b) => a.x !=null && a.y != null && a.x.equals(b.x) && a.y.equals(b.y)
     * </code>
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Tuple) {
            Tuple t = (Tuple) o;
            if (x == null || y == null)
                return false;
            return x.equals(t.x) && y.equals(t.y);
        }
        return super.equals(o);
    }
}
