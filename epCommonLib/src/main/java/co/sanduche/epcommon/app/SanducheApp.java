package co.sanduche.epcommon.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

import java.net.CookieHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import co.sanduche.epcommon.services.EPClient;
import co.sanduche.epcommon.services.EPCookieHandler;

/**
 * Base class for all apps that follow Sanduche model. This class overrides #onCreate and performs some default steps:<br />
 * 1. Defines and configures a default queue for EPClient<br/>
 * 2. Loads from the manifest the development phase<br/>
 * 3. Overrides the default android Uncaught Exception Manager <br/><br/>
 * <p/>
 * For this to work, the app MUST declare a meta tag inside the application element with key co.sanduche.common.DEV_PHASE and one of the values: <br/>
 * <code>@integer/dev_phase_development</code><br/>
 * <code>@integer/dev_phase_staging</code><br/>
 * <code>@integer/dev_phase_production</code><br/>
 */
public abstract class SanducheApp extends Application {

    public static String CACHE_DIR = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/.sanduche_tmp";

    public static final String DEV_PHASE_META_DATA_KEY = "co.sanduche.common.DEV_PHASE";
    public static final String AMAZON_S3_SERVICE_KEY = "AMAZON_S3_SERVICE_KEY";
    public static final String STRIPE_SERVICE_KEY = "STRIPE_SERVICE_KEY";

    private static DevPhase PHASE;

    protected static SanducheApp instance;

    private EPCookieHandler cookieHandler;

    public SanducheApp() {
        SanducheApp.instance = this;
    }


    public enum DevPhase {
        /** */
        Development,
        /** */
        Staging,
        /** */
        Production;

        public String server;
        public Map<String, String> services;

        public void config(String server, String... services) {
            if (services.length % 2 != 0)
                throw new IllegalArgumentException("The number of values in services is not correct. Key, value pairs do not match");
            HashMap<String, String> map = new HashMap<String, String>();
            for (int i = 0; i < services.length - 1; i += 2)
                map.put(services[i], services[i + 1]);
            config(server, map);
        }

        public void config(String server, Map<String, String> services) {
            this.server = server;
            this.services = services;
        }

        public Map<String, String> getServices() {
            return Collections.unmodifiableMap(services);
        }
    }

    public static String getServiceKey(String key){
        return PHASE.getServices().get(key);
    }

    public static void setPHASE(DevPhase PHASE) {
        SanducheApp.PHASE = PHASE;
    }

    @Override
    public void onCreate() {

        //EPClient Config
        Context context = getApplicationContext();
        cookieHandler = new EPCookieHandler(context);
        CookieHandler.setDefault(cookieHandler);

//        new File(CACHE_DIR).mkdirs();
        CACHE_DIR = getCacheDir().getAbsolutePath();

        configSanduche();

        //DevPhase loading
        try {
            Integer phaseI = getAppMetaData(DEV_PHASE_META_DATA_KEY, Integer.class, -1);
            PHASE = DevPhase.values()[phaseI];
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(
                    "La aplicacion no se puede inicializar: Fase de desarrollo incorrecta. Asegurese de declarar la fase de desarrollo en el manifest",
                    e);
        }

        EPClient.config(context);
        EPClient.setServerRoot(PHASE.server);

        //UEH Config
        androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(handler);

        //App creation
        onCreateSanduche();

        super.onCreate();
    }


    // ***********************************************//
    // Default Exception Handler
    // ***********************************************//
    private Thread.UncaughtExceptionHandler androidDefaultUEH = Thread
            .getDefaultUncaughtExceptionHandler();

    private Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            Log.e("UncaughtException",
                    "Excepcion no controlada en el thread " + thread + " ex:"
                            + ex.getMessage());
            ex.printStackTrace();
            androidDefaultUEH.uncaughtException(thread, ex);
        }
    };

    public static SanducheApp getInstance() {
        if (instance == null) {
            //This should never happen
            String msg = "La instancia de la aplicacion no ha sido definida. Asegurese de hacer super().instance = this en el metodo onCreateSanduche de su aplicacion";
            Log.e("SanducheApp", msg);
            RuntimeException e = new RuntimeException(msg);
            e.printStackTrace();
            throw e;
        }
        return instance;
    }

    //******************************************************************
    // HELPERS
    //******************************************************************

    public static SharedPreferences getGlobalSharedPreferences() {
        return getInstance().getApplicationContext()
                .getSharedPreferences(
                        getInstance().getApplicationInfo().name + "GlobalPrefs",
                        Context.MODE_PRIVATE);
    }

    public EPCookieHandler getCookieHandler() {
        return cookieHandler;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getGlobalPreference(String key, Class<T> clazz, T defValue) {
        SharedPreferences p = getInstance().getGlobalSharedPreferences();
        Object res = p.getAll().get(key);
        return res == null ? defValue : (T) res;
    }

    public static <T> void saveGlobalPreference(String key, T value) {
        Editor editor = getInstance().getGlobalSharedPreferences().edit();
        if (value == null || value.getClass() == String.class)
            editor.putString(key, (String) value);
        else if (value.getClass() == Boolean.class)
            editor.putBoolean(key, (Boolean) value);
        else if (value.getClass() == Float.class)
            editor.putFloat(key, (Float) value);
        else if (value.getClass() == Integer.class)
            editor.putInt(key, (Integer) value);
        else if (value.getClass() == Long.class)
            editor.putLong(key, (Long) value);

        editor.commit();
    }

    public static DevPhase getDevPhase() {
        return PHASE;
    }


    public static <T> T getAppMetaData(String key, Class<T> clazz, T defValue) {
        try {
            Context context = getInstance().getApplicationContext();
            ApplicationInfo ai = context.getPackageManager()
                    .getApplicationInfo(
                            context.getPackageName(),
                            PackageManager.GET_META_DATA);
            Object res = ai.metaData
                    .get(key);
            return res == null ? defValue : (T) res;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);

        }
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion() {
        try {
            Context context = getInstance().getApplicationContext();
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static String getAppVersionName() {
        try {
            Context context = getInstance().getApplicationContext();
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    //******************************************************************
    // Abstracts
    //******************************************************************

    /**
     * Called before onCreateSanduche, the all the phases in DevPhase must be configured
     */
    public abstract void configSanduche();

    /**
     * Equivalent for onCreate in Sanduche model
     */
    public abstract void onCreateSanduche();

}
