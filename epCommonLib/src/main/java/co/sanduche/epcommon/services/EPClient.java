package co.sanduche.epcommon.services;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.sanduche.epcommon.util.network.HttpUtils;


/**
 * Utility class that simplifies network interaction
 */
public class EPClient {

    public static void setHeaderProvider(HeaderProvider headerProvider) {
        EPClient.headerProvider = headerProvider;
    }

    /**
     * Request Methods. We use are own so the implementation details are encapsulated
     */
    public enum RequestMethod {
        GET(Request.Method.GET),
        POST(Request.Method.POST),
        PUT(Request.Method.PUT),
        DELETE(Request.Method.DELETE),
        HEAD(Request.Method.HEAD),
        OPTIONS(Request.Method.OPTIONS),
        TRACE(Request.Method.TRACE),
        PATCH(Request.Method.PATCH);

        private int value;

        RequestMethod(int value) {
            this.value = value;
        }
    }

//    public final static String AUTH_COOKIE = "PLAY_SESSION";

    private static String serverRoot;

    private static RequestQueue requestQueue;

    private static HeaderProvider headerProvider = new DefaultHeaderProvider();

    //**********************************************************
    // CONFIG
    //**********************************************************

    public static void config(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    public static void setServerRoot(String root) {
        if (EPClient.serverRoot == null)
            EPClient.serverRoot = root;
        else
            //Someone tried to change our serverRoot
            throw new IllegalStateException("Attempt to change server root after initialization");

    }

    //**********************************************************
    // HELPERS
    //**********************************************************

    public static String getAbsoluteUrl(String url) {
        if (url.contains("http") || url.contains("https"))
            return url;
        return serverRoot + url;
    }

    public static String getServerRoot() {
        return serverRoot;
    }

    //**********************************************************
    // LOW LEVEL API
    //**********************************************************

    public static <T> void addRequest(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(req);
    }

    public static <T> EPJsonRequest<T> createRequest(RequestMethod method, String url, Object body, Class<T> responseType, final EPCallback<T> callback, Object... urlParams) {

        String absoluteUrl = getAbsoluteUrl(HttpUtils.urlInterpolate(url, urlParams));
        callback.setAbsoluteUrl(absoluteUrl);
        EPJsonRequest<T> req = new EPJsonRequest<T>(method.value, absoluteUrl, body, responseType, callback);

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 1, 1.5f));
        return req;
    }

    public static <T> EPJsonListRequest<T> createListRequest(RequestMethod method, String url, Object body, Class<T> responseType, final EPCallback<List<T>> callback, Object... urlParams) {

        String absoluteUrl = getAbsoluteUrl(HttpUtils.urlInterpolate(url, urlParams));
        callback.setAbsoluteUrl(absoluteUrl);
        EPJsonListRequest<T> req = new EPJsonListRequest<T>(method.value, absoluteUrl, body, responseType, callback);
        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 1, 1.5f));
        return req;
    }

    public interface HeaderProvider {
        Map<String, String> getHeaders();
    }

    private static class DefaultHeaderProvider implements HeaderProvider {

        @Override
        public Map<String, String> getHeaders() {
            return new HashMap<String, String>();
        }
    }

    static void setHeaders(EPRequest<?> req) {
        Map<String, String> headers = headerProvider.getHeaders();
        try {
            Map<String, String> finalHeaders = req.getHeaders();
            for (String h : headers.keySet()) {
                finalHeaders.put(h, headers.get(h));
            }
        } catch (Exception authFailureError) {
            authFailureError.printStackTrace();
        }
    }

    //**********************************************************
    // API
    //**********************************************************

    public static EPJsonRequest<JSONObject> get(String url, final EPCallback<JSONObject> callback, Object... urlParams) {
        EPJsonRequest<JSONObject> req = createRequest(RequestMethod.GET, url, null, JSONObject.class, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

    public static <T> EPJsonRequest<T> get(String url, Class<T> responseType, final EPCallback<T> callback, Object... urlParams) {
        EPJsonRequest<T> req = createRequest(RequestMethod.GET, url, null, responseType, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

    public static EPJsonListRequest<JSONObject> getList(String url, final EPCallback<List<JSONObject>> callback, Object... urlParams) {
        EPJsonListRequest<JSONObject> req = createListRequest(RequestMethod.GET, url, null, JSONObject.class, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

    public static <T> EPJsonListRequest<T> getList(String url, Class<T> responseType, final EPCallback<List<T>> callback, Object... urlParams) {
        EPJsonListRequest<T> req = createListRequest(RequestMethod.GET, url, null, responseType, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }


    public static EPJsonRequest<JSONObject> post(String url, Object body, final EPCallback<JSONObject> callback, Object... urlParams) {
        EPJsonRequest<JSONObject> req = createRequest(RequestMethod.POST, url, body, JSONObject.class, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

    public static <T> EPJsonRequest<T> post(String url, Object body, Class<T> responseType, final EPCallback<T> callback, Object... urlParams) {
        return sendRequest(RequestMethod.POST, url, body, responseType, callback, urlParams);
    }


    public static EPJsonListRequest<JSONObject> postList(String url, Object body, final EPCallback<List<JSONObject>> callback, Object... urlParams) {
        EPJsonListRequest<JSONObject> req = createListRequest(RequestMethod.POST, url, body, JSONObject.class, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

    public static <T> EPJsonListRequest<T> postList(String url, Object body, Class<T> responseType, final EPCallback<List<T>> callback, Object... urlParams) {
        return sendListRequest(RequestMethod.POST, url, body, responseType, callback, urlParams);
    }

    /**
     * Creates and puts a request into the request queue.
     *
     * @param method    Request method
     * @param url       Endpoint
     * @param callback  Callback. Can be null
     * @param urlParams List of key value pairs i the form: <code>key1, value1, ... , keyN, valueN,</code>. <br/>
     * @return
     */
    public static EPJsonRequest<JSONObject> sendJSONRequest(RequestMethod method, String url, Object body, final EPCallback<JSONObject> callback, Object... urlParams) {
        EPJsonRequest<JSONObject> req = createRequest(method, url, body, JSONObject.class, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

    /**
     * Creates and puts a request into the request queue.
     *
     * @param method    Request method
     * @param url       Endpoint
     * @param callback  Callback. Can be null
     * @param urlParams List of key value pairs i the form: <code>key1, value1, ... , keyN, valueN,</code>. <br/> Params will be used to build a JSON object in the form <code>{ "key1":value1, ... , "keyN":valueN }</code> using JSONUtil.toJSON(Object... urlParams)
     * @return
     */
    public static <T> EPJsonRequest<T> sendRequest(RequestMethod method, String url, Object body, Class<T> responseType, final EPCallback<T> callback, Object... urlParams) {
        EPJsonRequest<T> req = createRequest(method, url, body, responseType, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

    public static <T> EPJsonListRequest<T> sendListRequest(RequestMethod method, String url, Object body, Class<T> responseType, final EPCallback<List<T>> callback, Object... urlParams) {
        EPJsonListRequest<T> req = createListRequest(method, url, body, responseType, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }


    /**
     * Creates and puts a request into the request queue.
     *
     * @param method    Request method
     * @param url       Endpoint
     * @param callback  Callback. Can be null
     * @param urlParams List of key value pairs i the form: <code>key1, value1, ... , keyN, valueN,</code>. <br/> Params will be used to build a JSON object in the form <code>{ "key1":value1, ... , "keyN":valueN }</code> using JSONUtil.toJSON(Object... urlParams)
     * @return
     */
    public static EPJsonListRequest<JSONObject> sendJSONListRequest(RequestMethod method, String url, Object body, final EPCallback<List<JSONObject>> callback, Object... urlParams) {
        EPJsonListRequest<JSONObject> req = createListRequest(method, url, body, JSONObject.class, callback, urlParams);
        setHeaders(req);
        addRequest(req);
        return req;
    }

}
