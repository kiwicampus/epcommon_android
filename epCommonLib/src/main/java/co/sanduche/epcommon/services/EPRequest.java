package co.sanduche.epcommon.services;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import co.sanduche.epcommon.uil.EPJson;

/**
 * Created by Yeisson on 19/03/2015.
 */
public abstract class EPRequest<T> extends Request<T> {
    protected Class<?> responseType;
    //    protected EPCallback<T> handler;
    protected String mRequestBody;
    protected String url;

    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";
    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE = String.format(
            "application/json; charset=%s", PROTOCOL_CHARSET);
    private HashMap<String, String> headers;

    /**
     * Creates a new request.
     *
     * @param method      the HTTP method to use
     * @param url         URL to fetch the JSON from
     * @param requestData A {@link Object} to post and convert into json as the request.
     *                    Null is allowed and indicates no parameters will be posted
     *                    along with request.
     * @param listener    Listener to receive the JSON response
     */

    public EPRequest(int method, String url, Object requestData,
                     Class<?> responseType, Response.ErrorListener listener) {
        super(method, url, listener);
        this.url = url;
        try {
            this.mRequestBody = requestData instanceof JSONObject || requestData instanceof JSONArray
                    ? requestData.toString()
//                    : JSON.std.asString(requestData);
                    : EPJson.toJson(requestData);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        this.handler = listener;
        this.responseType = responseType;
    }

    @Override
    public byte[] getPostBody() {
        return getBody();
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {
            return mRequestBody == null ? null : mRequestBody
                    .getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog
                    .wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (this.headers == null)
            this.headers = new HashMap<String, String>();
        Map<String, String> superHeaders = super.getHeaders();
        headers.putAll(superHeaders);
        return headers;
    }
}
