package co.sanduche.epcommon.services;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;

/**
 * Helper used when calling the server. It can be used with direct Volley requests and using EPClient
 *
 * @param <T>
 */
public abstract class EPCallback<T> implements Listener<T>, ErrorListener {

    protected String absoluteUrl;

    public void onErrorResponse(Exception e) {
        try {
            onFailure(e);
        } catch (IllegalStateException ex) {
            //Fail silently
            //This occurs when trying to display something after going to background
        }

    }

    @Override
    public void onErrorResponse(VolleyError e) {
        try {
            onFailure(e);
        } catch (IllegalStateException ex) {
            //Fail silently
            //This occurs when trying to display something after going to background
        }
        try {
            onAny();
        } catch (IllegalStateException ex) {
            //Fail silently
            //This occurs when trying to display something after going to background
        }

    }

    @Override
    public void onResponse(T data) {
        try {
            onSuccess(data);
        } catch (IllegalStateException e) {
            //Fail silently
            //This occurs when trying to display something after going to background
        }
        try {
            onAny();
        } catch (IllegalStateException e) {
            //Fail silently
            //This occurs when trying to display something after going to background
        }
    }

    protected abstract void onSuccess(T data);

    protected void onFailure(Exception e) {
        e.printStackTrace();
        Log.i("EPCallback", "Error requesting url: " + absoluteUrl);
        if (e instanceof VolleyError) {
            VolleyError ve = (VolleyError) e;
            NetworkResponse error = ve.networkResponse;
            if (error != null) {
                System.out.println("Network Error: " + error.statusCode);
                System.out.println("error.data = " + new String(error.data));
                System.out.println("error.headers = " + error.headers);
            }
        }
    }

    protected void onAny() {

    }

    public void setAbsoluteUrl(String absoluteUrl) {
        this.absoluteUrl = absoluteUrl;
    }
}
