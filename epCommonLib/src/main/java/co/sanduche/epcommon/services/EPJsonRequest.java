package co.sanduche.epcommon.services;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
//import com.fasterxml.jackson.jr.ob.JSON;

import org.json.JSONObject;

import co.sanduche.epcommon.uil.EPJson;

public class EPJsonRequest<T> extends EPRequest<T> {

    private final EPCallback<T> handler;

    public EPJsonRequest(int method, String url, Object requestData, Class<T> responseType, EPCallback<T> listener) {
        super(method, url, requestData, responseType, listener);
        this.handler = listener;
    }

    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        String jsonString = null;
        try {
            jsonString = response.data == null || response.data.length == 0 ? null : new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));

            T res;
            if (jsonString == null || responseType == Void.class)
                res = null;
            else if (responseType == JSONObject.class)
                res = (T) new JSONObject(jsonString);
            else
//                res = (T) JSON.std.beanFrom(responseType, jsonString);
                res = (T) EPJson.fromJson(jsonString, responseType);

            return Response.success(res, HttpHeaderParser.parseCacheHeaders(response));

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),
                    "Error when parsing response from url: " + url);
            Log.e(getClass().getSimpleName(), "JSON string: " + jsonString);
            Log.e(getClass().getSimpleName(), "Raw data: " + response.data);

            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        handler.onResponse(response);
    }
}