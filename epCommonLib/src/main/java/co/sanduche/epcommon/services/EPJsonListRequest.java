package co.sanduche.epcommon.services;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
//import com.fasterxml.jackson.jr.ob.JSON;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.sanduche.epcommon.uil.EPJson;

public class EPJsonListRequest<T> extends EPRequest<List<T>> {
    private final EPCallback<List<T>> handler;

    /**
     * Creates a new request.
     *
     * @param method       the HTTP method to use
     * @param url          URL to fetch the JSON from
     * @param requestData  A {@link Object} to post and convert into json as the request.
     *                     Null is allowed and indicates no parameters will be posted
     *                     along with request.
     * @param responseType
     * @param listener     Listener to receive the JSON response
     */
    public EPJsonListRequest(int method, String url, Object requestData, Class<T> responseType, EPCallback<List<T>> listener) {
        super(method, url, requestData, responseType, listener);
        this.handler = listener;
    }

    protected Response<List<T>> parseNetworkResponse(NetworkResponse response) {
        String jsonString = null;
        try {
            jsonString = response.data == null || response.data.length == 0 ? null : new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));

            List<T> res;
            if (jsonString == null || responseType == Void.class)
                res = new ArrayList<T>();

            else if (responseType == JSONObject.class) {
                JSONArray array = new JSONArray(jsonString);
                res = new ArrayList<T>();
                for (int i = 0; i < array.length(); i++)
                    res.add((T) array.getJSONObject(i));
            } else {
                res = (List<T>) EPJson.fromJsonArray(jsonString, responseType);

            }
            return Response.success(res, HttpHeaderParser.parseCacheHeaders(response));

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),
                    "Error when parsing response from url: " + url);
            Log.e(getClass().getSimpleName(), "JSON string: " + jsonString);
            Log.e(getClass().getSimpleName(), "Raw data: " + response.data);

            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(List<T> response) {
        if (handler != null)
            handler.onResponse(response);
    }
}