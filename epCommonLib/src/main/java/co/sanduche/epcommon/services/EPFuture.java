package co.sanduche.epcommon.services;

import com.android.volley.Request;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A Future that represents a Volley request.
 * 
 * Used by providing as your response and error listeners. For example:
 * 
 * <pre>
 * RequestFuture&lt;JSONObject&gt; future = RequestFuture.newFuture();
 * MyRequest request = new MyRequest(URL, future, future);
 * 
 * // If you want to be able to cancel the request:
 * future.setRequest(requestQueue.add(request));
 * 
 * // Otherwise:
 * requestQueue.add(request);
 * 
 * try {
 * 	JSONObject response = future.get();
 * 	// do something with response
 * } catch (InterruptedException e) {
 * 	// handle the error
 * } catch (ExecutionException e) {
 * 	// handle the error
 * }
 * </pre>
 * 
 * @param <T>
 *            The type of parsed response this future expects.
 */
public class EPFuture<T> extends EPCallback<T> implements Future<T> {
	private Request<?> mRequest;
	private boolean mResultReceived = false;
	private T mResult;
	private Throwable mException;

	public static <E> EPFuture<E> newFuture() {
		return new EPFuture<E>();
	}

	private EPFuture() {
	}

	public void setRequest(Request<?> request) {
		mRequest = request;
	}

	@Override
	public synchronized boolean cancel(boolean mayInterruptIfRunning) {
		if (mRequest == null) {
			return false;
		}
		if (!isDone()) {
			mRequest.cancel();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public T get() throws InterruptedException, ExecutionException {
		try {
			return doGet(null);
		} catch (TimeoutException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public T get(long timeout, TimeUnit unit) throws InterruptedException,
			ExecutionException, TimeoutException {
		return doGet(TimeUnit.MILLISECONDS.convert(timeout, unit));
	}

	private synchronized T doGet(Long timeoutMs) throws InterruptedException,
			ExecutionException, TimeoutException {
		if (mException != null) {
			throw new ExecutionException(mException);
		}
		if (mResultReceived) {
			return mResult;
		}
		if (timeoutMs == null) {
			wait(0);
		} else if (timeoutMs > 0) {
			wait(timeoutMs);
		}
		if (mException != null) {
			throw new ExecutionException(mException);
		}
		if (!mResultReceived) {
			throw new TimeoutException();
		}
		return mResult;
	}

	@Override
	public boolean isCancelled() {
		if (mRequest == null) {
			return false;
		}
		return mRequest.isCanceled();
	}

	@Override
	public synchronized boolean isDone() {
		return mResultReceived || mException != null || isCancelled();
	}

	@Override
	public synchronized void onFailure(Exception error) {
		mException = error;
		notifyAll();
//		onFailure(error);
	}

	@Override
	public synchronized void onSuccess(T data) {
		mResultReceived = true;
		mResult = data;
		notifyAll();
	}
}
