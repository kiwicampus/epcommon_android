package co.sanduche.epcommon.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import co.sanduche.epcommon.services.EPCallback;

/**
 * Created by Yeisson on 31/03/2015.
 * Custom callback. When onFailure called, it shows a default message
 * <p>"Server might not be available. Try again"</p>
 * <p/>
 * This message can be customized
 */
public abstract class EPUICallback<T> extends EPCallback<T> {

    private final Context context;
    private final String message;
    private final ProgressDialog progressDialog;

    public EPUICallback(Context c) {
        this(c, "Server is not available. You might want to check your network");
    }

    public EPUICallback(Context c, ProgressDialog progressDialog) {
        this(c, "Server is not available. You might want to check your network", progressDialog);
    }

    public EPUICallback(Context c, String message) {
        this(c, message, null);
    }

    public EPUICallback(Context c, String message, ProgressDialog progressDialog) {
        this.context = c;
        this.message = message;
        this.progressDialog = progressDialog;
    }

    @Override
    public void onResponse(T arg0) {
        super.onResponse(arg0);
        safeDismiss();
    }

    @Override
    public void onErrorResponse(Exception e) {
        super.onErrorResponse(e);
        safeDismiss();
    }

    @Override
    public void onFailure(Exception exception) {
        exception.printStackTrace();
        Log.i("Error listener", "Error invocando el servidor: " + exception.getMessage());
        Log.i("Error listener", "Requested URL: " + absoluteUrl);
        try {
            String m = parseException(exception);
            Toast.makeText(context, m, Toast.LENGTH_LONG).show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void safeDismiss() {
        try {
            if (progressDialog != null)
                progressDialog.dismiss();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public String parseException(Exception exception) {
        String m = message;
        if (exception instanceof VolleyError) {
            VolleyError volleyError = (VolleyError) exception;
            if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                String networkData = new String(volleyError.networkResponse.data);
                try {
                    JSONObject networkJson = new JSONObject(networkData);
                    m = networkJson.getString("error");
                } catch (JSONException e) {
                    m = networkData;
                }
            }
        }
        return m;
    }
}
