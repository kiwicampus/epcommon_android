package co.sanduche.epcommon.gcm;

//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.gcm.GoogleCloudMessaging;

import co.sanduche.epcommon.app.SanducheApp;

/**
 * Created by Yeisson on 13/04/2015.
 * <p/>
 * Registers a device to receive GCM notifications. For the registration to work, you must include a meta-data key in the AndroidManifest in this way
 * <p/>
 * <b>Important!!</b> Project Id MUST be a String
 * <p/>
 * <code>
 * &lt;application&gt;<br/>
 * <PRE>&lt;meta-data
 * android:name="co.sanduche.epcommon.gcm.PROJECT_ID";
 * android:value="YOUR_PROJECT_ID" /&gt;;<br/></PRE>
 * &lt;/application&gt;
 * </code>
 * <p/>
 */
public class GCMRegistrationHelper {

//    public static final String PROJECT_ID_META_DATA_KEY = "co.sanduche.epcommon.gcm.PROJECT_ID";
//
//    public static final String PROPERTY_REG_ID = "co.sanduche.epcommon.gcm.registration_id";
//    private static final String PROPERTY_APP_VERSION = "co.sanduche.epcommon.gcm.appVersion";
//    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
//
//    /**
//     * Tag used on log messages.
//     */
//    static final String TAG = "GCMRegistrationHelper";
//
//    public static void registerGCM(Activity context, GCMRegistrationListener listener) {
//        // Check device for Play Services APK. If check succeeds, proceed with
//        //  GCM registration.
//        if (checkPlayServices(context)) {
//
//            registerInBackground(context, listener);
//        } else {
//            Log.i(TAG, "No valid Google Play Services APK found.");
//        }
//    }
//
//    /**
//     * Check the device to make sure it has the Google Play Services APK. If
//     * it doesn't, display a dialog that allows users to download the APK from
//     * the Google Play Store or enable it in the device's system settings.
//     *
//     * @param context
//     */
//    public static boolean checkPlayServices(Activity context) {
//        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//                GooglePlayServicesUtil.getErrorDialog(resultCode, context,
//                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
//            } else {
//                Log.i(TAG, "This device is not supported.");
////                finish(); //TODO ????
//            }
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * Gets the current registration ID for application on GCM service.
//     * <p/>
//     * If result is empty, the app needs to register.
//     *
//     * @return registration ID, or empty string if there is no existing
//     * registration ID.
//     */
//    private static String getRegistrationId() {
//        String registrationId = SanducheApp.getInstance().getGlobalPreference(PROPERTY_REG_ID, String.class, "");
//        if (registrationId.isEmpty()) {
//            Log.i(TAG, "Registration not found.");
//            return "";
//        }
//        // Check if app was updated; if so, it must clear the registration ID
//        // since the existing registration ID is not guaranteed to work with
//        // the new app version.
//        int registeredVersion = SanducheApp.getGlobalPreference(PROPERTY_APP_VERSION, Integer.class, Integer.MIN_VALUE);
//        int currentVersion = SanducheApp.getAppVersion();
//        if (registeredVersion != currentVersion) {
//            Log.i(TAG, "App version changed.");
//            return "";
//        }
//        return registrationId;
//    }
//
//    /**
//     * Registers the application with GCM servers asynchronously.
//     * <p/>
//     * Stores the registration ID and app versionCode in the application's
//     * shared preferences.
//     *
//     * @param listener
//     */
//    private static void registerInBackground(final Context context, final GCMRegistrationListener listener) {
//        new AsyncTask<Void, String, String>() {
//            public Exception exception;
//
//            @Override
//            protected String doInBackground(Void... params) {
//
//                String regId;
//                regId = getRegistrationId();
//
//                if (!regId.isEmpty())
//                    return regId;
//
//                String SENDER_ID = SanducheApp.getAppMetaData(PROJECT_ID_META_DATA_KEY, String.class, null).toString();
//                if (SENDER_ID == null)
//                    throw new RuntimeException("meta-data key " + PROJECT_ID_META_DATA_KEY + " has not been defined in AndroidManifest");
//                Log.i(TAG, "Loaded SENDER_ID: " + SENDER_ID);
//
//                try {
//                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
//                    Log.i(TAG, "Inicia registro GCM");
//                    regId = gcm.register(SENDER_ID);
//                    Log.i(TAG, "RegID se obtiene de GCM. Enviando a servidor...");
//
//                    listener.sendRegistrationIdToBackendSynchronously(regId);
//                    Log.i(TAG, "RegID registrado en servidor");
//
//                    // Persist the registration ID - no need to register again.
//                    storeRegistrationId(regId);
//                } catch (Exception ex) {
//                    this.exception = ex;
//                    // If there is an error, don't just keep trying to register.
//                    // Require the user to click a button again, or perform
//                    // exponential back-off.
//
//                    //TODO See if this error can be handled
//                }
//                return regId;
//            }
//
//            @Override
//            protected void onProgressUpdate(String... values) {
//                onPostExecute(values[0]);
//            }
//
//            @Override
//            protected void onPostExecute(String msg) {
//                if (listener != null) {
//                    if (exception != null)
//                        listener.onFailure(exception);
//                    else
//                        listener.onSuccess(msg);
//                }
//
//            }
//        }.execute(null, null, null);
//    }
//
//    /**
//     * Stores the registration ID and app versionCode in the application's
//     * {@code SharedPreferences}.
//     *
//     * @param regId registration ID
//     */
//    private static void storeRegistrationId(String regId) {
//        SanducheApp.saveGlobalPreference(PROPERTY_REG_ID, regId);
//        SanducheApp.saveGlobalPreference(PROPERTY_APP_VERSION, SanducheApp.getAppVersion());
//    }
//
//    public static abstract class GCMRegistrationListener extends EPCallback<String> {
//        public abstract void sendRegistrationIdToBackendSynchronously(String regID) throws Exception;
//    }
}
