package co.sanduche.epcommon.util.network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yeisson on 17/03/2015.
 */
public class JSONUtil {

    public static JSONObject toJSON(Object... params) {
        try {
            Map<String, Object> map = toMap(params);
            if(map==null)
                return null;
            JSONObject obj = new JSONObject();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String k = entry.getKey();
                Object p = entry.getValue();
                if (p instanceof List) {
                    p = new JSONArray((List) p);
                }
                obj.put(k, p);
            }
            return obj;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Object> toMap(Object... params) {
        if (params == null || params.length == 0)
            return null;
        if (params.length % 2 != 0)
            throw new IllegalArgumentException("Params provided are not correct. Keys and values do not match");

        HashMap<String, Object> res = new HashMap();

        for (int i = 0; i < params.length - 1; i += 2) {
            String k = params[i].toString();
            Object p = params[i + 1];
            res.put(k, p);
        }
        return res;
    }
}
