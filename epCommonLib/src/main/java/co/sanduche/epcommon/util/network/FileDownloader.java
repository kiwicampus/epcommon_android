package co.sanduche.epcommon.util.network;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import co.sanduche.epcommon.services.EPCallback;
//import co.sanduche.epcommon.uil.SanducheImageLoader;

/**
 * Created by Yeisson on 29/04/2015.
 */
public class FileDownloader {

    public static void downloadFile(String url,
                                    String destination, EPProgressListener<String> callback) {
        new DownloadTask(url, destination, callback).execute();
    }

    public static String downloadFileSync(String sUrl, String destination, EPProgressListener listener) {
        File outputFile = new File(destination);
        if (outputFile != null && outputFile.exists())
            return outputFile.getAbsolutePath();

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(outputFile);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            long iniTime = System.currentTimeMillis();
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                if (fileLength > 0 && listener != null) // only if total length is known
                    listener.onProgressUpdate(total * 100 / fileLength);
                long readTime = System.currentTimeMillis() - iniTime;
                double secs = (double) readTime / 1000.0;
                double bytesPerSecond = count / secs;
                if (listener != null)
                    listener.onSpeedUpdate(bytesPerSecond);
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return outputFile.getAbsolutePath();
    }

    protected static class DownloadTask extends AsyncTask<String, Double, String> {

        private final EPProgressListener<String> callback;
        private final String url;
        private final String destination;
        private Exception exception;

        public DownloadTask(String url, String destination, EPProgressListener<String> callback) {
            this.callback = callback;
            this.url = url;
            this.destination = destination;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return downloadFileSync(url, destination, new EPProgressListener() {
                    @Override
                    public void onProgressUpdate(double progress) {
                        publishProgress(progress);
                    }
                });
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Double... values) {
            callback.onProgressUpdate(values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            if (exception != null) {
                callback.onErrorResponse(exception);
                return;
            }
            callback.onResponse(s);
        }
    }


    public abstract static class EPProgressListener<T> extends EPCallback<T> {
        public abstract void onProgressUpdate(double progress);

        @Override
        public void onSuccess(T data) {

        }

        public void onSpeedUpdate(double bytesPerSecond) {
            Log.v("ProgressListener", "Downloading at: " + bytesPerSecond / 1000 + "KB/s");
        }
    }

}
