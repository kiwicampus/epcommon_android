package co.sanduche.epcommon.db;

/**
 * Created by Caeus on 4/28/2015.
 */

import android.util.Log;


import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import co.sanduche.epcommon.app.SanducheApp;

/**
 * Created by Caeus on 4/27/2015.
 */
public class SnappySingleton {

    private final static Object mLock = new Object();
    private static DB db;

    public static <R> R execute(String dbName, SnappyTransaction<R> function) throws SnappydbException {
        synchronized (mLock) {
            Log.v(SnappySingleton.class.getSimpleName() + ".execute", "\"OPENS\" = " + "OPENS");
            db = getDB(dbName);
            R r = function.apply(db);
            Log.v(SnappySingleton.class.getSimpleName() + ".execute", "\"OPENS\" = " + "CLOSE");
            db.close();
            return r;
        }
    }

    public static void destroy(String dbName) throws SnappydbException {
        synchronized (mLock) {
            Log.v(SnappySingleton.class.getSimpleName() + ".execute", "\"OPENS\" = " + "OPENS");
            db = getDB(dbName);
            db.destroy();
            Log.v(SnappySingleton.class.getSimpleName() + ".execute", "\"OPENS\" = " + "CLOSE");
        }
    }

    private static DB getDB(String name) throws SnappydbException {
        if (db != null && db.isOpen()) db.close();
        if (name != null)
            db = DBFactory.open(SanducheApp.getInstance().getApplicationContext(), name);
        else
            db = DBFactory.open(SanducheApp.getInstance().getApplicationContext());
        return db;
    }



}
