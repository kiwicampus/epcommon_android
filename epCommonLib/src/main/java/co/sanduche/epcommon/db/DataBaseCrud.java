package co.sanduche.epcommon.db;

import java.util.List;

/**
 * Created by jasonoviedo on 6/19/16.
 */
public class DataBaseCrud<T extends Storable> extends BaseDataBaseCrud {
    private Class<T> clazz;

    public DataBaseCrud(String dbName, Class<T> clazz) {
        super(dbName);
        this.clazz = clazz;
    }

    /**
     * Lists items by prefix
     *
     * @param prefix
     * @return
     */
    public List<T> getListByPrefix(final String prefix) {
        return getListByPrefix(prefix, clazz);
    }

    /**
     * Lists all items
     *
     * @return
     */
    public List<T> getAll() {
        return getAll(clazz);
    }

    public T get(final String key) {
//        return execute(new SnappyTransaction<T>() {
//            @Override
//            public T apply(DB db) throws SnappydbException {
//                return get(db, key, clazz);
//            }
//        }, dbName);
        return get(key, clazz);
    }
}
