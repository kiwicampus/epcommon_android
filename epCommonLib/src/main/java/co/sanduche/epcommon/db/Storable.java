package co.sanduche.epcommon.db;

/**
 * Created by jasonoviedo on 6/18/16.
 */
public interface Storable {
    String getDataBasePrefix();

    String getDataBaseId();
}
