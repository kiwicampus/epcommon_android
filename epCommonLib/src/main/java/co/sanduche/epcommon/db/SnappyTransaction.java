package co.sanduche.epcommon.db;

import com.snappydb.DB;
import com.snappydb.SnappydbException;


/**
 * Created by Caeus on 4/29/2015.
 */
public interface SnappyTransaction<R> {

    public R apply(DB db) throws SnappydbException;

}
