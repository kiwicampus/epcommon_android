package co.sanduche.epcommon.db;

import android.support.annotation.NonNull;

import com.snappydb.DB;
import com.snappydb.KeyIterator;
import com.snappydb.SnappydbException;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for storing info in data base
 * Created by jasonoviedo on 6/20/16.
 */
public class BaseDataBaseCrud {
    private String dbName;

    //**************************************************************
    // Public API
    //**************************************************************

    public BaseDataBaseCrud(String dbName) {
        this.dbName = dbName;
    }


    public <T extends Storable> void setList(final List<T> list) {
        execute(new SnappyTransaction<Void>() {

            @Override
            public Void apply(DB db) throws SnappydbException {
                KeyIterator keyIterator = db.allKeysIterator();
                for (String[] keys : keyIterator.byBatch(100)) {
                    for (String key : keys) {
                        db.del(key);
                    }
                }
                keyIterator.close();
                for (T object : list) {
                    putStorableInner(db, object);
                }
                return null;
            }
        }, dbName);
    }


    public <T> T safeGet(final String key, final Class<T> clazz) {
        try {
            return get(key, clazz);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param key Key used to save the entry with
     * @param clazz Clazz to deserialize from
     * @param <T> Type
     * @return Objects
     * @throws RuntimeException
     */
    public <T> T get(final String key, final Class<T> clazz) {
        return execute(new SnappyTransaction<T>() {
            @Override
            public T apply(DB db) throws SnappydbException {
                return db.exists(key) ? db.getObject(key, clazz) : null;
            }
        }, dbName);
    }

    public void removeAll(final String prefix) {
        execute(new SnappyTransaction<Void>() {
            @Override
            public Void apply(DB db) throws SnappydbException {
                KeyIterator keyIterator = db.findKeysIterator(prefix);
                for (String[] keys : keyIterator.byBatch(100)) {
                    for (String key : keys) {
                        db.del(key);
                    }
                }
                return null;
            }
        }, dbName);
    }

    public void truncateDB() {
        try {
            SnappySingleton.destroy(dbName);
        } catch (SnappydbException e) {
            throw new RuntimeException(e);
        }
    }

    public void save(final Storable object) {
        execute(new SnappyTransaction<Void>() {
            @Override
            public Void apply(DB db) throws SnappydbException {
                putStorableInner(db, object);
                return null;
            }
        }, dbName);
    }

    public void save(final String key, final Object object) {
        execute(new SnappyTransaction<Void>() {
            @Override
            public Void apply(DB db) throws SnappydbException {
                if (object == null)
                    remove(db, key);
                else
                    db.put(key, object);
                return null;
            }
        }, dbName);
    }

    public void update(final Storable object) {
        save(object);
    }

    public void remove(Storable o) {
        save(getDBKey(o), null);
    }


    public <T extends Storable> List<T> getListByPrefix(final String prefix, final Class<T> clazz) {
        return execute(new SnappyTransaction<List<T>>() {
            @Override
            public List<T> apply(DB db) throws SnappydbException {
                return getListByPrefixInner(db, prefix, clazz);
            }
        }, dbName);
    }

    public <T extends Storable> List<T> getAll(final Class<T> clazz) {
        return execute(new SnappyTransaction<List<T>>() {

            @Override
            public List<T> apply(DB db) throws SnappydbException {
                return getAllInner(db, clazz);
            }
        }, dbName);
    }

    //**************************************************************
    // Helpers
    //**************************************************************

    private static <R> R execute(SnappyTransaction<R> function, String dbName) {
        try {
            return SnappySingleton.execute(dbName, function);
        } catch (SnappydbException e) {
            throw new RuntimeException(e);
        }
    }

    private <T extends Storable> void putStorableInner(DB db, T m) throws SnappydbException {
        if (m == null)
            return;

        String dbKey = getDBKey(m);
        db.put(dbKey, m);
    }

    @NonNull
    private <T extends Storable> String getDBKey(T m) {
        String prefix = m.getDataBasePrefix() == null ? "" : m.getDataBasePrefix();
        String id = m.getDataBaseId();
        return prefix + id;
    }

    /**
     * Lists all items in the data base
     *
     * @param db
     * @param clazz
     * @return
     * @throws SnappydbException
     */
    private <T extends Storable> List<T> getAllInner(DB db, Class<T> clazz) throws SnappydbException {
        List<T> objects = new ArrayList<T>();
        KeyIterator keyIterator = db.allKeysIterator();
        for (String[] keys : keyIterator.byBatch(100)) {
            for (String key : keys) {
                objects.add(db.getObject(key, clazz));
            }
        }
        keyIterator.close();
        return objects;
    }

    private <T extends Storable> List<T> getListByPrefixInner(DB db, String prefix, Class<T> clazz) throws SnappydbException {
        List<T> objects = new ArrayList<T>();
        String[] keys = db.findKeys(prefix);
        System.out.println("keys.length = " + keys.length);
        for (String key : keys) {
            objects.add(db.getObject(key, clazz));
        }
        return objects;
    }

    private void remove(DB db, String key) throws SnappydbException {
        db.del(key);
    }

}
